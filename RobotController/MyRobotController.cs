﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotController {

    public struct MyQuat {

        public float w;
        public float x;
        public float y;
        public float z;

        //Constructor
        public MyQuat(float a, float b, float c, float d) {
            w = a;
            x = b;
            y = c;
            z = d;
        }
    }

    public struct MyVec {

        public float x;
        public float y;
        public float z;

        //Constrtuctor
        public MyVec(float a, float b, float c) {
            x = a;
            y = b;
            z = c;
        }
    }

    public class MyRobotController {

        //Exercicis
        #region public methods

        //Booleans neccesaris per els exercicis 1 i 2
        bool animacio = true;
        bool animacio2 = false;

        float counter = 0.0f;
        float counter1 = 0.0f;
        float counter2 = 0.0f;

        public string Hi() {
            string s = "hello world from my Robot Controller";
            return s;

        }

        //EX1: this function will place the robot in the initial position

        public void PutRobotStraight(out MyQuat rot0, out MyQuat rot1, out MyQuat rot2, out MyQuat rot3) {

            MyQuat temp = NullQ;

            MyVec eixX = new MyVec(1.0f, 0.0f, 0.0f);
            MyVec eixY = new MyVec(0.0f, 1.0f, 0.0f);
            MyVec eixZ = new MyVec(0.0f, 0.0f, 1.0f);

            rot0 = Rotate(temp, eixY, 73.0f);
            rot1 = Multiply(Rotate(temp, eixY, 73.0f), Rotate(temp, eixX, -20.0f));
            rot2 = Multiply(Rotate(temp, eixY, 74.5f), Rotate(temp, eixX, 85.0f));
            rot3 = Multiply(Rotate(temp, eixY, 73.0f), Rotate(temp, eixX, 110.0f));
            animacio2 = false;
        }

        //EX2: this function will interpolate the rotations necessary to move the arm of the robot until its end effector collides with the target (called Stud_target)
        //it will return true until it has reached its destination. The main project is set up in such a way that when the function returns false, the object will be droped and fall following gravity.

        public bool PickStudAnim(out MyQuat rot0, out MyQuat rot1, out MyQuat rot2, out MyQuat rot3) {

            rot0 = NullQ;
            rot1 = NullQ;
            rot2 = NullQ;
            rot3 = NullQ;

            if (!animacio2) {
                if (animacio) {
                    MyQuat temp = NullQ;

                    MyVec eixX = new MyVec(1.0f, 0.0f, 0.0f);
                    MyVec eixY = new MyVec(0.0f, 1.0f, 0.0f);
                    MyVec eixZ = new MyVec(0.0f, 0.0f, 1.0f);

                    rot0 = Rotate(temp, eixY, 73.0f);
                    rot1 = Multiply(Rotate(temp, eixY, 73.0f), Rotate(temp, eixX, -20.0f));
                    rot2 = Multiply(Rotate(temp, eixY, 74.5f), Rotate(temp, eixX, 85.0f));
                    rot3 = Multiply(Rotate(temp, eixY, 73.0f), Rotate(temp, eixX, 110.0f));
                    animacio = false;
                    return true;
                }

                bool myCondition = true;

                if (counter < 40) {
                    counter += 0.3f;
                }
                else if ( counter2 < 40) {
                    counter2 += 0.3f;
                }
                if (counter1 < 30 && counter2 >= 20) {
                    counter1 += 0.3f;
                }

                if (counter2 >= 40 && counter1 >= 30) {
                    animacio2 = true;
                    counter = 0f;
                    counter1 = 0f;
                    counter2 = 0f;
                    return false;
                }

                if (myCondition) {

                    MyQuat Joint;

                    Joint.w = (float)Math.Cos(GrausaRadians_ex2(75 - counter)); 
                    Joint.x = 0;
                    Joint.y = (float)Math.Sin(GrausaRadians_ex2(75 - counter));
                    Joint.z = 0;

                    rot0 = Normalize(Joint);

                    MyQuat Joint2;

                    Joint2.w = (float)Math.Cos(GrausaRadians_ex2(-30 + counter1));
                    Joint2.x = (float)Math.Sin(GrausaRadians_ex2(-30 + counter1));
                    Joint2.y = 0;
                    Joint2.z = 0;

                    rot1 = Multiply(rot0, Joint2);

                    MyQuat Joint3;

                    Joint3.w = (float)Math.Cos(GrausaRadians_ex2(125 - counter2));
                    Joint3.x = (float)Math.Sin(GrausaRadians_ex2(125 - counter2));
                    Joint3.y = 0;
                    Joint3.z = 0;

                    rot2 = Multiply(rot1, Joint3);

                    MyQuat Joint4;

                    Joint4.w = (float)Math.Cos(GrausaRadians_ex2(15));
                    Joint4.x = (float)Math.Sin(GrausaRadians_ex2(15));
                    Joint4.y = 0;
                    Joint4.z = 0;

                    rot3 = Multiply(rot2, Joint4);

                    return true;
                }
            }
            return false;
        }

        //EX3: this function will calculate the rotations necessary to move the arm of the robot until its end effector collides with the target (called Stud_target)
        //it will return true until it has reached its destination. The main project is set up in such a way that when the function returns false, the object will be droped and fall following gravity.
        //the only difference wtih exercise 2 is that rot3 has a swing and a twist, where the swing will apply to joint3 and the twist to joint4

        public bool PickStudAnimVertical(out MyQuat rot0, out MyQuat rot1, out MyQuat rot2, out MyQuat rot3) {

            rot0 = NullQ;
            rot1 = NullQ;
            rot2 = NullQ;
            rot3 = NullQ;

            return false;
        }

        //Neccesari Exercici 3
        public static MyQuat GetSwing(MyQuat rot3) { return NullQ;}

        public static MyQuat GetTwist(MyQuat rot3) { return NullQ; }

        #endregion

        //Funcions neccesaries per els exercicis
        #region private and internal methods

        //Variables per les funcions
        private const float Pi = 3.1415f;

        private static MyQuat NullQ {
            get {
                MyQuat a;
                a.w = 1;
                a.x = 0;
                a.y = 0;
                a.z = 0;

                return a;
            }
        }

        internal MyQuat Multiply(MyQuat Q1, MyQuat Q2) {

            MyQuat temp;

            temp.w = Q1.w * Q2.w - Q1.x * Q2.x - Q1.y * Q2.y - Q1.z * Q2.z;
            temp.x = Q1.w * Q2.x + Q1.x * Q2.w + Q1.y * Q2.z - Q1.z * Q2.y;
            temp.y = Q1.w * Q2.y + Q1.y * Q2.w + Q1.z * Q2.x - Q1.x * Q2.z;
            temp.z = Q1.w * Q2.z + Q1.z * Q2.w + Q1.x * Q2.y - Q1.y * Q2.x;

            return Normalize(temp);
        }

        internal MyQuat Rotate(MyQuat currentRotation, MyVec axis, float angle) {

            //Conversio de graus a radians (goddamit unity)
            float radians = GrausaRadians(angle);

            currentRotation.w = (float)Math.Cos(radians / 2);
            currentRotation.x = axis.x * (float)Math.Sin(radians / 2);
            currentRotation.y = axis.y * (float)Math.Sin(radians / 2);
            currentRotation.z = axis.z * (float)Math.Sin(radians / 2);

            return Normalize(currentRotation);
        }

        internal float GrausaRadians (float angle) {return angle*(Pi/180);}

        //Versio neccesaria per l'exercici 2
        internal float GrausaRadians_ex2(float angle) {return angle*(Pi/180)/2;}

        //Neccesari per normalitzar quaternions
        internal float Length(MyQuat Q) { return (float)Math.Abs(Math.Pow(Q.w, 2) + Math.Pow(Q.x, 2) + Math.Pow(Q.y, 2) + Math.Pow(Q.z, 2)); }

        //Neccesari per totes les interaccions finals amb quaternions
        internal MyQuat Normalize(MyQuat Q){
            float l = Length(Q);
            if (l > 0.0f) { return new MyQuat(Q.w/l, Q.x/l, Q.y/l, Q.z/l);
            }
            else { return Q; }
        }

        #endregion
    }
}
